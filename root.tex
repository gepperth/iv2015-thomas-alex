%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%2345678901234567890123456789012345678901234567890123456789012345678901234567890
%        1         2         3         4         5         6         7         8

\documentclass[letterpaper, 10 pt, conference]{ieeeconf}  % Comment this line out if you need a4paper

%\documentclass[a4paper, 10pt, conference]{ieeeconf}      % Use this line for a4 paper

\IEEEoverridecommandlockouts                              % This command is only needed if 
                                                          % you want to use the \thanks command

\overrideIEEEmargins                                      % Needed to meet printer requirements.

% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document

% The following packages can be found on http:\\www.ctan.org
\usepackage{graphics} % for pdf, bitmapped graphics files
\usepackage{epsfig} % for postscript graphics files
\usepackage{makecell}
\usepackage[utf8]{inputenc}
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{times} % assumes new font selection scheme installed
%\usepackage{amsmath} % assumes amsmath package installed
%\usepackage{amssymb}  % assumes amsmath package installed

\title{\LARGE \bf
A light-weight real-time applicable hand gesture recognition system for automotive applications
}


\author{Thomas Kopinski$^{1}$, Stéphane Magand$^{1}$, Alexander Gepperth$^{2}$ and Uwe Handmann$^{1}$% <-this % stops a space
\thanks{*This work was not supported by any organization}% <-this % stops a space
\thanks{$^{1}$Thomas Kopinski, Stéphane Magand and Uwe Handmann are with the Department of Informatics, University Ruhr West
        {\tt\small firstname.lastname@hs-rw.de}}%
\thanks{$^{2}$Alexander Gepperth is with the ENSTA ParisTech
        {\tt\small b.d.researcher@ieee.org}}%
}


\begin{document}



\maketitle
\thispagestyle{empty}
\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}

We present a novel approach for improved hand-gesture recognition by a single time-of-flight(ToF) sensor in an automotive environment. As the sensor's lateral resolution is  comparatively low, we employ a learning approach comprising multiple processing steps, including PCA-based cropping, the computation of robust point cloud descriptors and training of a Multilayer perceptron (MLP) on a large database of samples. A sophisticated temporal fusion technique boosts the overall robustness of recognition by taking into account data coming from previous classification steps. Overall results are very satisfactory when evaluated on a large benchmark set of ten different hand poses, especially when it comes to generalization on previously unknown persons.
%
\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\label{sec:introduction}
%Context
Being able to interact with a system naturally is becoming ever more important in many fields of Human-Computer-Interaction (HCI). Free-hand poses and gestures are one means which are attributed in this way, thus developing new interaction techniques is desirable in every-day situations, especially with declining sensor costs. Depending on the environment there are parameters restricting the applicability of these devices. Therefore, any approach has to be tailored to its needs. In the automotive environment, factors as daylight interference, obstacle occlusion, limited accessibility are only some of the problems which require consideration. 

%What we do
We present an approach to recognize hand poses with a single ToF-camera mounted on the center console. The so-called point clouds coming from the camera are transformed into a histogram, which in turn is used as input for the training and classification of the hand pose by a Multilayer-Perceptron (MLP). 
Our approach is purely data-driven in that all relevant information comes from a large database containing samples belonging to 1 out of 10 different hand poses. 
The use of a PCA-based hand cropping technique and robust point cloud descriptors, together with a neural network-based multi-class learning approach, make our system invariant to rotation, translation and deformation issues, which moreover works without the need to formulate a possibly complicated hand model. Using a ToF sensor additionally provides robustness against daylight interferences. 

Building upon prior results\cite{kopinskineural,kopinski2014real}, we extend our database to have more variance in the data (by adding more persons), and present a novel temporal fusion technique which boosts recognition rates by taking into account preceding recognitions as well. Moreover, our temporal fusion of data lets us take an initial step towards defining dynamic gestures via static hand poses by taking into consideration several snapshots taken over time.\\

We will first discuss the related work relevant for our research (Sec.~\ref{sec:relatedwork}). We then go on to describe the parametrization of the sensors and the setup of our system within an automobile environment in Sec.~\ref{section:systemsetup}. Subsequently we describe the recorded database in Sec.~\ref{section:database} and afterwards give an outline of the PCA algorithm used for the cropping of the point clouds in the database as well as in real-time (Sec.~\ref{section:pca}). In Sec.~\ref{section:descriptors} we give an account of the used different holistic point cloud descriptors and explain the meaning of the parameter variations we will test. Sec. \ref{sec:temporalintegration} describes the temporal fusion technique as well as the parametrization of the MLPs. 
The key questions we will investigate in Sec.~\ref{section:experiments} concern the generalization error of the MLP and the performance of our system in a live demonstration as well as offline. Lastly in ~\ref{section:outlook} we give an outlook of potential improvements of our system as well as our next steps. 
%
\section{Related work}
\label{sec:relatedwork}

Depth sensors represent an easy and robust solution for recognising hand poses, as they can easily deal with tasks such as the segmentation of the hand/arm from the body by simple thresholding as described in \cite{oprisescu2012automatic}. Several surveys have made use of this feature with various approaches to segmentation. Moreover it is possible to make use of depth information to distinguish ambiguous hand postures \cite{kollorz2008gesture}. Nevertheless, it has not been possible to achieve satisfactory results utilising only a single depth sensor. Either the range of application was limited or the performance results were dissatisfying. Usually a good performance result was achieved with a very limited pose set or if designed for a specific application \cite{soutschek20083}. However, for our purposes we need to employ several hand poses which are partly very difficult to disambiguate. 
ToF-Sensors - although working at stereo-frame rate - generally suffer from a low resolution which of course makes it difficult to extract robust yet informative features. Improved results can be achieved when fusing Stereo Cameras with Depth Sensors, e.g. in \cite{wen2012robust}. In \cite{kapuscinski2014hand} a single ToF-Sensor is used to detect hand postures with the Viewpoint Feature Histogram.

Various approaches make use of the Kinect sensor's ability to extract depth and RGB data simultaneously \cite{tang2011recognizing}. However this approach relies heavily on finding hand pixels in order to be able to segment the hand correctly. Moreover, approaches utilising the Kinect sensor will always suffer from direct illumination  by sunlight, which is not an issue for us as ToF-sensors show robust results in such situations. 
The authors of \cite{oikonomidis2011efficient} equally make use of the Kinect sensor's ability to acquire RGB and depth data simultaneously albeit using a hand model as a basis for hand pose detection. Nevertheless this algorithm relies on finding skin-coloured pixels as well, to allow for segmentation in 2D and 3D as well as tracking the hand.
To our knowledge there is no comparable work which is placed in the automotive environment. An extensive overview of the methods and applications used for hand gesture recognition is provided in \cite{suarez2012hand}. One of their insights is that most applications are in the field of robot control, interactive displays/tabletops/whiteboards or sign language recognition. In \cite{riener2011natural} a case study is made of how the Kinect sensor can be utilised to control E-Mail functions in a car through set of six hand gestures. While the results remain unclear, except for the fact that gestures could be well accepted as a means of control in a car, the gesture set remains small and the effect of different lighting conditions on the results is not discussed. 
More comprehensive overviews are given in \cite{ren2011depth} and \cite{wachs2011vision}.

Besides these technological issues, considerable research is conducted on how to design intuitive user interfaces, potentially based on 
hand postures and gestures. In \cite{bailly2011comparing}, the authors investigate and compare different menu techniques, whereas
 \cite{wilson2010combining} presents a system using several projectors and depth cameras named "LightSpace". 
The user is surrounded by surfaces which are filled with content by the projectors. He can interact with the walls, tables, etc. by gestures as these are recognized based by several cameras from the Kinect.
Special use cases and fields of applications are considered by several authors. In medical environments, touch gestures are not applicable for reasons of hygiene. Alternative touch-free approaches are explored in \cite{johnson2011exploring} and \cite{ruppert2012touchless}.

In-car scenarios have been in the focus of development for several years, as the driver can keep his hands close to the steering wheel while being able to focus on the surrounding environment. Pointing capabilities could be interesting to control content in the head-up displays. A good overview of automotive HMI is given in \cite{pickering2007research}. Hassani describes advantages in user acceptance of in-air hand gestures in comparison to touch gestures seniors of Human-Robot Interaction  \cite{znagui2011touch}.

Such scenarios demand robust data extraction techniques which is provided by the aforementioned ToF-sensor.
Our approach shows that it is possible to achieve satisfactory results relying solely on depth data when detecting various hand poses. In merging information from previous timesteps we are able to boost our results significantly while always retaining the applicability under various lighting conditions - one of the greatest advantages of ToF-sensors compared to e.g. the frequently used Kinect sensor.	 

\section{System Setup}
\label{section:systemsetup}

\begin{figure}
	\centering
	\includegraphics[width=0.49\textwidth]{../images/camboardnano.eps}
	\caption{The camboard nano}
	\label{figure:camboardnano}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.49\textwidth]{../images/demo1.eps}
	\caption{Driver interacting with the system.}
	\label{figure:interaction}
\end{figure}


We integrate a single ToF Camera (the Camboard nano - see Fig.\ref{figure:camboardnano}) into a car, fix it to the center console and connect it to a Laptop with a Linux system installed, handling the computation task. No calibration is necessary other than the exposure time set to 0.8ms and optimized for close-range interaction. The camera itself has comparably small dimensions (37x30x25mm) and measures distances by the time-of-flight principle which makes it useful in any outdoor scenario. The benefit of our approach is the fact that little to no calibration is necessary as the robustness of our descriptor and the neural networks compensate for minor changes in camera setup. The camera is recording the designated inner part of the car interior in which the driver is able to interact with the system (see Fig.\ref{figure:interaction}). We focus on recognising the subject's (i.e. driver) right hand for the desired hand poses. Therefore we defined the designated Volume of Interest (VOI) within which we want to identify user input. Due to driver behaviour, possible range and convenience as well as obstacle occlusion (e.g. steering wheel) our VOI is of trapezoidal shape with a depth of 27-35cm, a maximum width of 60cm and a minimum width of about 45cm enclosed by the Field of View(FoV) of the camera frustum. The total height covered by our camera ranges from 30-35cm. Furthermore we cover a space big enough to recognize the most important movements in the car. Usually the driver has his hand on the steering wheel or close to it or in other situations leans onto the armrest, which differs significantly in position and allows for longer interaction with our system. By defining our VOI as described, we are able to cover all of these possibilities.  

\section{Hand Gesture Database}
\label{section:database}
%
%
%
\begin{figure*}
	\centering
	\includegraphics[width=1.0\textwidth]{../images/gestures2.eps}
	\caption{The hand gesture database consisting of 10 different static hand poses.}
	\label{figure:db}
\end{figure*}
%
We record data from 16 persons, each displaying 10 different hand poses (cf. Figure \ref{figure:db}). For each gesture, 3000 samples are recorded, summing up to 30000 samples per person and a total database of 480000 samples. In order to induce some variance into the data, during the recording phase each participant is asked to rotate and translate their hand in all possible directions. Moreover, to tackle the task of scaling, for each gesture we define 3 different distance ranges, in which the participant is asked to perform the hand gesture in order to ensure sufficient sample coverage for various distances. Each frame is recorded at a resolution of 165x120px at 90fps with the Camboard nano, making it robust to daylight interferences and thus applicable in any outdoor scenario. This results in an alphabet of ten hand poses: Counting from 1-5 and \textit{fist}, \textit{stop}, \textit{grip}, \textit{L}, \textit{point} denoted by \textit{a-j} (cf. Figure \ref{figure:db}). For the chosen probands, both male and female, the size of the hand ranges from 8,5cm - 9,5cm in width and from 17,0cm - 19,5cm in length. 
%
%


\section{Hand-forearm segmentation with PCA}
\label{section:pca}
%
%
\subsection{Finding the principal axis of a point cloud }
%
%~~  \\
The main directions of the cloud are found using Principal Component Analysis (PCA) \cite{jolliffe2005principal}.
%
%This method has been described by Ian Jolliffe in "Principle Component analysis", published on Oct. 10th 2005.
PCA aims to find uncorrelated basis vectors for an arbitrary set of data vectors. Eigenvectors (also termed "principal components") are ordered by the variance of data points projected onto them, allowing efficient data compression by omitting principal components 
of low variance. 
This algorithm is applied as shown below, using as input the set of $n$ 3D coordinates of points in a point cloud denoted $x_j$, $j \in [0, n ]$).
%
\begin{itemize}
\item The mean value $ \bar x = {1 \over n} \cdot  \sum_{j=1}^{n} (x_j) $ is computed.
\item The scatter matrix is calculated : $$ S =  \sum_{j=1}^{n} (x_j - \bar x)(x_j - \bar x)^\top $$
This matrix can be used as maximum-likelihood estimate of the covariance matrix.
\item The Eigenvectors of this matrix yield the principal components. 
\end{itemize}

\begin{figure}
	\centering
	\includegraphics[width=0.49\textwidth]{../images/pcacrop2.eps}
	\caption{Point cloud before PCA-cropping (left) and after (right).}
	\label{figure:pca}
\end{figure}

We intend to cut off 'unnecessary' parts of the cloud, i.e. outliers and elongated parts of the forearm. In this case, the principal components correspond to orthogonal vectors that represent the most important directions in the point cloud. The vector with the most important y-component allows to recognize the axis hand-forearm.

The wrist, as the link between the hand and the forearm, is detected in order to determine a limit for the cropping. The employed method assumes that the distance between the endpoint of the fingers and the centroid is an upper bound of the distance between the centroid and the wrist.

To find the endpoint of the hand towards the direction of the fingers, tests are made along the axis, starting at the centroid and moving progressively upward. At each step, we determine whether there are points within a designated small neighborhood around the axis. The upper end of the hand is marked if this number of neighboring points equals 0. Then the bottom limit for the wrist is fixed at the same distance from the centroid, but in the inversed direction along the y-axis. All points below this wrist limit are cut out which is exemplarily shown in Fig.\ref{figure:pca}.
%
%
%



\section{The PFH-Descriptor}
\label{section:descriptors}
The PFH-Descriptor (PFH-Histogram) \cite{rusu2008aligning} is a local descriptor which relies on the calculation of normals. It is able to capture the geometry of a requested point for a defined k-neighbourhood. Thus, for a query point and another point within its neighbourhood, four values (the point features or PFs) are being calculated, three of which are angle values and the fourth being the euclidean distance between these two points. The angle components are influenced by each point's normal, so in order to be able to calculate them, all the normals have to be calculated for all points in the cloud. Therefore we are able to capture geometric properties of a point cloud in a sufficient manner, depending on the chosen parameters. These parameters have been thoroughly examined in our previous work which led for example to an optimal choice for the parameter \textit{n}, the radius for calculation of the sphere which encloses all points used to calculate the normal of a query point. One major drawback is the fact that the PFH-descriptor cannot be easily embedded into a real-time applicable system as the computation cost becomes too high, when we extend it to be a global descriptor. To overcome this issue, we present a modification of the PFH-Descriptor in the following section. 
%
\subsection{Modification of the PFH-Descriptor}
\label{methods:lcc}
Our version of the PFH-Descriptor makes use of its descriptive power while maintaining the real-time applicability. Using the PFH in a global sense would mean having to enlarge the radius so that every two point pairs in the cloud are used to create the descriptor. This quickly results in a quadratically scaling computation problem as a single PFH-calculus would have to be performed 10000 times for a point cloud of 100 points. Given the fact that our point clouds have a minimum size of 200 points up to 2000 points and more, this is not feasible for our purposes. Therefore we randomly choose 10000 point pairs and use the quantized PFs to build a global 625-dimensional histogram. We calculate one descriptor per point cloud which forms the input for the neural network.


\section{Training setup and MLP parametrization}
\label{sec:temporalintegration}
\subsection{Temporal integration of information}

%
In order to improve the overall recognition rate of our MLP-based approach, we present a temporal fusion technique. To this end the training and testing data have to be prepared as follows: Firstly, we split training and testing data for each run in such a way that data coming from one person is not included in the training set, thus being able to measure the generalization performance of our system on previously unknown data. Secondly the training data has to be presented in a chronological way in order to make the fusion technique viable. The overall procedure is split into a 2-step approach. In an initial step an MLP is trained on all the data from the training set. To induce temporal information into the second MLP, the training step has to be modified in such a way that, at a given point in time $t$, not only the input from the feature vector is presented to the MLP, but also the values of the output neurons of the first MLP classifying the sample at time $t-1$. To achieve this, the training data for MLP 2 have to be presented in a chronological order. Therefore the size of the input layer of the second MLP is determined by the length of the feature vector + number of the output neurons of MLP 1 and for our case sums up to (625 + 10). This approach can be  motivated as follows: During the interaction of the user with the system, multiple snapshots are taken from the camera for a single hand pose. Thus information considered 'over time' i.e. for classifications coming time points shortly before the current point in time can be used to stabilize the results. 
%
%
\subsection{Neural network topology and training parameters}
%
For the described two-step fusion approach the networks are trained with standard parameters with variations on the network topology. The input for the first network is formed by the modified point feature histogram(MPFH) of a processed point cloud as described in Sec.~\ref{sec:temporalintegration}. The input for the second network is formed by first classifying the previous sample with the first network, calculating the output neurons' activities, and then concatenating these activities with the MPFH of the current time step. Thus, the input layer of the second network is of size $n + 10$, since both networks have as many output neurons as there are classes in the classification problem, i.e., 10. The network topologies are therefore $625-h-10$ and $635-h-10$, respectively, with variations possible in the hidden layer size $h$. We conduct several experiments to obtain good values for $h$, which we find in the range of 30-50 hidden neurons.

The network is implemented using the FANN library \cite{nissen2003implementation}. The training algorithm is the standard RPROP algorithm and the activation function is the sigmoid function for both hidden and output layers.  
%
%
\subsection{Real-time applicability} 
%
Our current system is able to perform feature extraction and classification in real-time. More specifically, the segmentation and cropping of the hand, the calculation of features and the classification task are realisable at a frame rate of 30-50Hz, depending on the size of the point cloud. 

\section{Experiments and results}

\label{section:experiments}
\begin{table*}
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|l|}
\hline
participant & 1  & 2  & 3  & 4  & 5  & 6  & 7  & 8  & 9  & 10 & 11 & 12 & 13 & 14 & 15 & 16 & Accuracy\\ \Xhline{2\arrayrulewidth}
MLP1 (30 N)& 81\% & 50\% & 69\% & 55\% & 76\% & 59\% & 56\% & 68\% & 79\% & 68\% & 88\% & 72\% & 95\% & 72\% & 87\% & 79\% & 72.12\% \\ \hline
MLP2 (30 N)& 83\% & 51\% & 72\% & 60\% & 79\% & 62\% & 58\% & 72\% & 85\% & 74\% & 89\% & 73\% & 97\% & 75\% & 91\% & 83\% & 75.25\% \\ \Xhline{3\arrayrulewidth}%\hline
MLP1 (50 N)& 83\% & 49\% & 69\% & 58\% & 79\% & 62\% & 57\% & 68\% & 84\% & 70\% & 89\% & 75\% & 90\% & 74\% & 90\% & 80\% & 73.94\% \\ \hline
MLP2 (50 N)& 86\% & 52\% & 74\% & 63\% & 83\% & 65\% & 60\% & 72\% & 89\% & 74\% & 90\% & 75\% & 98\% & 75\% & 93\% & 82\% & 76.94\% \\ \hline

\end{tabular}
\label{tab:results}
\caption{Generalization results for all 16 persons and both MLPs, each with 30 and 50 neurons respectively}
\end{table*}
%
%
%
%
%
We perform tests on the data set comprising all 16 persons. In order to compare our approach for the regular MLP and the fusion technique, two classifications are conducted for any input at a given point in time. Moreover, we vary the number of neurons in the hidden layer to measure whether we are able to improve the performance of our algorithms. Tab. \ref{tab:results} sums up the most important results. Each row shows classification accuracy for an MLP trained on all the data except the person it is tested on, each test being  a generalization performance test. As an example, column 1 then represents the performance of all four MLPs, trained on persons 2-16 and tested on person 1. The overall classification accuracy of an MLP - averaged over all persons - depending on the number of neurons in the hidden layer and the kind of fusion technique is shown in the last column.

First of all, it is observable that our fusion approach, denoted MLP2, outperforms the regular approach (MLP1) as the average MSE for the fused input is about 3\% lower averaged over all persons. For some individual cases we are able to improve the results by up to 8\% - here averaged over all gestures of a single person - as for instance for person 13 (improved from 90\% to 98\%) in the case of the larger MLP. Increasing the number of neurons from 30 to 50 has a negligible effect as the improvement of our approach compared to the standard MLP stays around 3\% for all variations of the sizes of the hidden layer. However, it is possible to lower the average overall MSE by around 1.5\%  from 14.75\% down to 13.06\%, when comparing the upper two rows of Tab. \ref{tab:results} with the lower rows.\\
%
%
\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{../images/p9_net1_n50.eps}
	\caption{Confusion matrix for the standard MLP.}
	\label{figure:conf1}
\end{figure}
%
%

%
%
\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{../images/p9_net2_n50.eps}
	\caption{Confusion matrix for the fused MLP.}
	\label{figure:conf2}
\end{figure}
For those cases which perform worst in terms of recognition rate, we are still able to improve the results by an average of 2-3\% by the fusion technique. It is only for three test persons that our fusion approach has little to no effect (persons 11, 12 and 14) as the change of MSE remains around 1\% or below. On the other hand, in no case the fusion approach leads to a deterioration of performance. 
A more precise evaluation of the results shows that the temporal fusion approach reduces the disambiguation problem to two candidates.

More specifically, the confusion matrices (cf. Fig.\ref{figure:conf1} and Fig.\ref{figure:conf2}) show the correct classifications as well as the false positives, for all ten gestures, here presented exemplarily for person 13. Each row represents the (mis-) classifications for each gesture shown on the left-hand border. So gesture A ('one') has been classified 2451 times (Fig.\ref{figure:conf1}) correctly with the standard approach and 2801 times correctly with the improved fusion technique (Fig.\ref{figure:conf1}). At the same time, the same gesture has been mistaken 217 times for gesture 'two' and this number has been lowered to 105 with the improved approach. Simultaneously the number of false positives for this gesture being mistaken for gesture I dropped from 243 to 81.\\
Moreover, both approaches work best for gestures D, F, G, and H nearing 100\% recognition rate, followed by gesture A, B and J (around 90\%) and C and I (around 75-80\%) referring to the standard MLP in Fig.\ref{figure:conf1}. For this person, both approaches perform worst for gesture E ('five') being classified correctly with a precision of around 55\% and 65\% respectively. More interestingly, it is observable that this gesture is being mistaken for gesture G ('flat hand') which can be explained by the similarity of appearance of both kinds of gestures.

Furthermore, the gesture J ('point') was mistaken around 201 times (cf. Fig.\ref{figure:conf1}) for gesture F ('fist') before our fusion approach and 81 times (Fig.\ref{figure:conf2}) after fusion. This is a drop of more than 60\% and it reduces the problem to disambiguating only gestures J and F from each other, which we are able to handle individually and makes the recognition task much easier. Similar observations can be made for gesture pairs B $\rightarrow D$ and E $\rightarrow$ D for the example depicted as well as across the group of participants as a whole.\\   
%
%
%
\section{Outlook}
\label{section:outlook}
%
In this article, we present a real-time hand gesture recognition system based on inexpensive and robust time-of-flight cameras, intended for human-machine interaction in an automotive environment. Tests on an offline database show excellent generalisation performance for a set of 10 static gestures. Our system works with an average frequency of 30-50Hz being limited mainly by the ToF-camera itself, depending primarily on the number of points in the captured point cloud. We are able to improve our previous system by reducing the required sensors to 1 while maintaining stable results on previously unknown data. We added the PCA algorithm to crop superfluous parts of the forearm which stabilizes the results even further as we assumed this was a major part of the disambiguation problem so far. Our approach fuses information coming from classifications made before the current point in time which, as we are able to prove, provides even more stability to our system.\\

The next steps consist of adding a disambiguation module for the most difficult cases as well as using our fusion technique to extend our recognition from static hand poses to dynamic hand gestures. Since we are able to reduce the disambiguation problem with our current approach to two cases in most situations this will result in a more stable recognition module. Furthermore we can add even more information over an extended time span as well as adding confidence measures to our current decision module. We intend to fuse our algorithms to create an improved gesture recognition system by allowing interaction via static hand poses and dynamic hand gestures.
%
%
\bibliographystyle{unsrt}
\bibliography{publications}
%
\end{document}
